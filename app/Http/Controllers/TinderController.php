<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TinderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tinder = new \Pecee\Http\Service\Tinder('100013333950207', 'EAAGm0PX4ZCpsBADbxWUz8KpO7mCjq8cdec7dRe7FKLWqgOZCUwCxtYiL1aDj2hBiATZBTsJZB98K6kdaHvzSWxCXFTuEk4WRMUZCARZCZAZCXRtfP3tyZBTkjTG3YhIXmrEg6rqcUmtHAFZAb5WAccirFA5oWI9IiEWuUG8YxY8t57SqOb7ATmXQRfHoexIYSb0nQIRGROaJchF7mA6VZB6CXvzhTkMsoZA6HeYZD');
        print_r($tinder);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
